## coheat.co.uk 

- development [![build status](https://gitlab.com/coheat/coheat.co.uk/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/coheat.co.uk/badges/master/build.svg)]() 
 
## comms-server 

- 0.0.3 [![build status](https://gitlab.com/coheat/comms-server/badges/0.0.3/build.svg)]() 
 
- 0.0.2 [![build status](https://gitlab.com/coheat/comms-server/badges/0.0.2/build.svg)]() 
 
- 0.0.1 [![build status](https://gitlab.com/coheat/comms-server/badges/0.0.1/build.svg)]() 
 
- custom_subject [![build status](https://gitlab.com/coheat/comms-server/badges/custom_subject/build.svg)]() 
 
- development [![build status](https://gitlab.com/coheat/comms-server/badges/development/build.svg)]() 
 
- jobserver [![build status](https://gitlab.com/coheat/comms-server/badges/jobserver/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/comms-server/badges/master/build.svg)]() 
 
## job-manager 

- 0.0.4 [![build status](https://gitlab.com/coheat/job-manager/badges/0.0.4/build.svg)]() 
 
- 0.0.3 [![build status](https://gitlab.com/coheat/job-manager/badges/0.0.3/build.svg)]() 
 
- 0.0.2 [![build status](https://gitlab.com/coheat/job-manager/badges/0.0.2/build.svg)]() 
 
- 0.0.1 [![build status](https://gitlab.com/coheat/job-manager/badges/0.0.1/build.svg)]() 
 
- development [![build status](https://gitlab.com/coheat/job-manager/badges/development/build.svg)]() 
 
- fail_sequential_actions [![build status](https://gitlab.com/coheat/job-manager/badges/fail_sequential_actions/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/job-manager/badges/master/build.svg)]() 
 
- use_new_session_mgmt [![build status](https://gitlab.com/coheat/job-manager/badges/use_new_session_mgmt/build.svg)]() 
 
## manual-payment 

- master [![build status](https://gitlab.com/coheat/manual-payment/badges/master/build.svg)]() 
 
## organization_registration 

- master [![build status](https://gitlab.com/coheat/organization_registration/badges/master/build.svg)]() 
 
## project-template 

- master [![build status](https://gitlab.com/coheat/project-template/badges/master/build.svg)]() 
 
## statement 

- 0.0.1 [![build status](https://gitlab.com/coheat/statement/badges/0.0.1/build.svg)]() 
 
- development [![build status](https://gitlab.com/coheat/statement/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/statement/badges/master/build.svg)]() 
 
## stripe-service 

- 0.0.2 [![build status](https://gitlab.com/coheat/stripe-service/badges/0.0.2/build.svg)]() 
 
- 0.0.1 [![build status](https://gitlab.com/coheat/stripe-service/badges/0.0.1/build.svg)]() 
 
- development [![build status](https://gitlab.com/coheat/stripe-service/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/stripe-service/badges/master/build.svg)]() 
 
## test_gear 

- development [![build status](https://gitlab.com/coheat/test_gear/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/test_gear/badges/master/build.svg)]() 
 
## user-service 

- development [![build status](https://gitlab.com/coheat/user-service/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/user-service/badges/master/build.svg)]() 
 
## user-webapp 

- development [![build status](https://gitlab.com/coheat/user-webapp/badges/development/build.svg)]() 
 
- master [![build status](https://gitlab.com/coheat/user-webapp/badges/master/build.svg)]() 
 
